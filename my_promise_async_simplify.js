class Promise {
  constructor(executor) {
    try {
      executor(resolve, reject)
    } catch (e) {
      reject(e)
    }
  }

  state = 'pending'
  value = null
  reason = null
  onResolvedCallbacks = []
  onRejectedCallbacks = []

  resolve = (value) => {
    if (this.state === 'pending') {
      this.state = 'fulfilled'
      this.onResolvedCallbacks.forEach(f => f())
    }
    this.value = value
  }

  reject = (reason) => {
    if (this.state === 'pending') {
      this.state = 'rejected'
      this.onRejectedCallbacks.forEach(f => f())
    }
    this.reason = reason
  }

  then(onFulfilled, onRejected) {
    onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : value => value
    onRejected = typeof onFulfilled === 'function' ? onRejected : err => { throw err }

    let promise2 = new Promise((resolve, reject) => {
      if (this.state === 'fulfilled') {
        setTimeout(() => {
          try { // then里内置的try-catch块
            let x = onFulfilled(this.value)
            resolve(x)
          } catch (e) {
            reject(e)
          }
        }, 0)
      } else if (this.state === 'rejected') {
        setTimeout(() => { // A+规定onFulfilled或onRejected不能被同步调用，必须被异步调用
          try {
            let x = onRejected(this.reason)
            resolve(x)
          } catch (e) {
            reject(e)
          }
        }, 0)
      } else { // pending
        this.onResolvedCallbacks.push(() => {
          setTimeout(() => {
            try {
              let x = onFulfilled(this.value)
              resolve(x)
            } catch (e) {
              reject(e)
            }
          }, 0)
        })
        this.onRejectedCallbacks.push(() => {
          setTimeout(() => {
            try {
              let x = onRejected(this.reason)
              resolve(x)
            } catch (e) {
              reject(e)
            }
          }, 0)
        })
      }
    })
    return promise2
  }
}