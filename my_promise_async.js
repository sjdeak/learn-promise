/**
 * 异步版增加了存放onFulfill和onRejected的队列
 */
class Promise {
  constructor(executor) {
    try {
      executor(resolve, reject)
    } catch (e) {
      reject(e)
    }
  }

  state = 'pending'
  value = null
  reason = null
  onResolvedCallbacks = []
  onRejectedCallbacks = []

  resolve = (value) => {
    if (this.state === 'pending') {
      this.state = 'fulfilled'
      this.onResolvedCallbacks.forEach(f => f())
    }
    this.value = value
  }

  reject = (reason) => {
    if (this.state === 'pending') {
      this.state = 'rejected'
      this.onRejectedCallbacks.forEach(f => f())
    }
    this.reason = reason
  }

  then(onFulfilled, onRejected) {
    if (this.state === 'fulfilled') {
      onFulfilled(this.value)
    } else if (this.state === 'rejected') {
      onRejected(this.reason)
    } else { // pending
      this.onResolvedCallbacks.push(() => {
        onFulfilled(this.value) // 注册登记，以后再调用
      })
      this.onRejectedCallbacks.push(() => {
        onRejected(this.reason)
      })
    }
  }
}
