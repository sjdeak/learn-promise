let a = (() => {
  return a
})()

function resolvePromise(promise2, x, resolve, reject) {
  if (x === promise2) {
    return reject(new TypeError('Chaining cycle detected for promise'))
  }

  let called
  if (x !== null && (typeof x === 'object' || typeof x === 'function')) {
    try {
      let then = x.then
      if (typeof then === 'function') {
        // x是thenable
        then.call(x, (y) => {
          if (called) return
          called = true
          resolvePromise(promise2, y, resolve, reject)
        }, (err) => {
          if (called) return
          called = true
          reject(err)
        })
      } else { // x只是普通对象或函数
        resolve(x)
      }
    } catch (e) {
      if (called) return
      called = true
      reject(e)
    }
  } else {
    resolve(e)
  }
}

/**
 * 异步版增加了存放onFulfill和onRejected的队列
 */
class Promise {
  constructor(executor) {
    try {
      executor(resolve, reject)
    } catch (e) {
      reject(e)
    }
  }

  state = 'pending'
  value = null
  reason = null
  onResolvedCallbacks = []
  onRejectedCallbacks = []

  resolve = (value) => {
    if (this.state === 'pending') {
      this.state = 'fulfilled'
      this.onResolvedCallbacks.forEach(f => f())
    }
    this.value = value
  }

  reject = (reason) => {
    if (this.state === 'pending') {
      this.state = 'rejected'
      this.onRejectedCallbacks.forEach(f => f())
    }
    this.reason = reason
  }

  then(onFulfilled, onRejected) {
    onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : value => value
    onRejected = typeof onFulfilled === 'function' ? onRejected : err => { throw err }

    let promise2 = new Promise((resolve, reject) => {
      if (this.state === 'fulfilled') {
        setTimeout(() => {
          try { // then里内置的try-catch块
            let x = onFulfilled(this.value)
            resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            reject(e)
          }
        }, 0)
      } else if (this.state === 'rejected') {
        setTimeout(() => { // 秘籍规定onFulfilled或onRejected不能被同步调用，必须被异步调用
          try {
            let x = onRejected(this.reason)
            resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            reject(e)
          }
        }, 0)
      } else { // pending
        this.onResolvedCallbacks.push(() => {
          setTimeout(() => {
            try {
              let x = onFulfilled(this.value)
              resolvePromise(promise2, x, resolve, reject)
            } catch (e) {
              reject(e)
            }
          }, 0)
        })
        this.onRejectedCallbacks.push(() => {
          setTimeout(() => {
            try {
              let x = onRejected(this.reason)
              resolvePromise(promise2, x, resolve, reject)
            } catch (e) {
              reject(e)
            }
          }, 0)
        })
      }
    })
    return promise2
  }
}



