class Promise {
  constructor(executor) {
    try {
      executor(resolve, reject)
    } catch (e) {
      reject(e)
    }
  }

  state = 'pending'
  value = null
  reason = null

  resolve = (value) => {
    if (this.state === 'pending') {
      this.state = 'fulfilled'
    }
    this.value = value
  }

  reject = (reason) => {
    if (this.state === 'pending') {
      this.state = 'rejected'
    }
    this.reason = reason
  }

  // 现在是同步版，如果executor是异步函数，那then的时候可能还是pending状态，onFulfilled, onRejected也不会被保存
  then(onFulfilled, onRejected) {
    if (this.state === 'fulfilled') {
      onFulfilled(this.value)
    } else if (this.state === 'rejected') {
      onRejected(this.reason)
    }
  }
}

