/*
Fix or improve the following snippets.
*/

/*
Pay attention to the `!` marks.
*/

/*
1. Case
*/

let getA = (p) => p.then(x => x + 1) // !
let getB = (p) => p.then(x => x * 2) // !
let getC = (p) => p.then(x => x % 3) // !

Promise.resolve(0).then(x => x + 1).then(x => x * 2).then(x => x % 3).then(console.log).catch(console.log)

/*
2. Case
*/

let getA = () => Promise.resolve("A")
let getB1 = (a) => Promise.resolve(a + "B1")
let getB2 = (a) => Promise.resolve(a + "B2")
let getC = (b1, b2) => Promise.resolve(b1 + b2 + "C")

getA() // !!! change below this line
  .then(a => {
    getB1(a) // Promise.resolve(a + "B1")
    getB2(b) // Promise.resolve(a + "B2")
  })
  .then((a, b1, b2) => {
    getC(b1, b2)
  })

getA().then(getB1).then(getB2).

/*
3. Case
*/

Promise.resolve(a)
  .then(a => 1 + a) // b = a + 1 !
  .then(b => 2 * b) // c = 2 * b !

/*
4. Case
*/

let ps = [
  Promise.resolve(1), Promise.resolve(2), Promise.resolve(3)
] // some array of promises...
//
// Promise.all([
//   ps[0].then(f), ps[1].then(f), ps[2].then(f), // !
// ]).then(g)

Promise.all(ps).then(_ => _.map(f)).then(g)
