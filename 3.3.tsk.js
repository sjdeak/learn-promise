/*
Getting a state (or a fate) of a promise is not possible technically speaking but we can trick a
system if we want to. Most of the time it's not a pattern you want to use, but it's a curios exercise
to learn more about promises anyway.
*/

/*
Write a function `promiseState` that takes a promise and returns its state/fate (as a string constant).
*/

function promiseState(p) {
  let t1 = new Date()
  p.then(_ => {
    let t2 = new Date()
  }, _ => {
    let
  })

  return ??? "pending"
         ??? "fullfilled"
         ??? "rejected"
}

let x = Promise.resolve("!")
let y = Promise.reject("?")
let z = new Promise(_ => null)

promiseState(x).then(console.log) // fulfilled
promiseState(y).then(console.log) // rejected
promiseState(z).then(console.log) // pending

/*
Hint: consider `Promise.race` to catch a pending state.
*/
