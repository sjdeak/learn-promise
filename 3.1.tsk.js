/*
Promises A+ are also weird in a sense that their side effects are called from a constructor. In general,
it's considered a bad design even by OOP merits. To make the use of promises most often you'll put
them in functions to restore laziness. It's not a perfect fix (as you'll see later) but it's something.
*/

/*
1. Write a function `now` that returns a promise, resolving to a current date.
*/

function now() {
  return Promise.resolve(new Date())
}

/*
2. Write a function `delay` that returns a promise, resolving in `t` milliseconds.
*/

function delay(t) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, t)
  })
}

delay(1000).then(_ => delay(1000)).then(_ => console.log("!")) // --!-->

/*
3. Write a function `dice` that returns a promise, resolving to `true` in `p` % of cases and
to `false` in `100 - p` %.xxxx
*/

function randomInteger(min, max) {
  return Math.round(min - 0.5 + Math.random() * (max - min + 1))
}

function chance(p) {
  return new Promise((resolve, reject) => {
    const rn = randomInteger(0, 1)

    if (rn < p / 100) {
      resolve(true)
    } else {
      resolve(false)
    }
  })
}

chance(50).then(_ => console.log(_)) // --!-->
